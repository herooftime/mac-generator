// Joseph Carpinelli
// mac-generator
// June 2, 2017

// Eventually Add String and File Support
// Break into functions
// Hexadecimal proper conversion (?)

// Info on hex conversion.
/* COPY PASTE HELP
Use <iomanip>'s std::hex. If you print, just send it to std::cout, if not, then use std::stringstream

std::stringstream stream;
stream << std::hex << your_int;
std::string result( stream.str() );

You can prepend the first << with << "0x" or whatever you like if you wish.

Other manips of interest are std::oct (octal) and std::dec (back to decimal).

One problem you may encounter is the fact that this produces the exact amount of digits needed to represent it. You may use setfill and setw this to circumvent the problem:

stream << std::setfill ('0') << std::setw(sizeof(your_type)*2)
       << std::hex << your_int;

So finally, I'd suggest such a function:

template< typename T >
std::string int_to_hex( T i )
{
  std::stringstream stream;
  stream << "0x"
         << std::setfill ('0') << std::setw(sizeof(T)*2)
         << std::hex << i;
  return stream.str();
}

*/

#include <iostream>
#include <random>
#include <ctime>

const int MAC_LENGTH = 12;
const int HEXDEC_LENGTH = 15;

int htQuit();
// Pre:
// Post:

int main()
{
	char ans;

	std::cout << "Generate MAC? Y/N: ";
	std::cin >> ans;
	std::cout << std::endl;

	while (ans == 'y' || ans == 'Y')
	{
		static std::default_random_engine r(time(NULL));
		std::uniform_int_distribution<int> genNum(0, HEXDEC_LENGTH);

		int mac[MAC_LENGTH] = {};

		for (int i = 0; i < MAC_LENGTH; i++)
		{
			mac[i] = genNum(r);
		}

		int i = 0;

		while (i < MAC_LENGTH)
		{
			for (int j = 0; j < 2; j++)
			{
				switch (mac[i])
				{
					case 10:
						std::cout << 'A';
						break;
					case 11:
						std::cout << 'B';
						break;
					case 12:
						std::cout << 'C';
						break;
					case 13:
						std::cout << 'D';
						break;
					case 14:
						std::cout << 'E';
						break;
					case 15:
						std::cout << 'F';
						break;
					default:
						std::cout << mac[i];
						break;
				}

				i++;
			}

			if (i < MAC_LENGTH)
			{
				std::cout << ":";
			}
		}

		std::cout << "\n\nRun again? Y/N: ";
		std::cin >> ans;
		std::cout << std::endl;
	}

	return htQuit();
}

int htQuit()
{
	char c;

	std::cout << "Enter 'q' to quit: ";
	std::cin >> c;

	return 0;
}
